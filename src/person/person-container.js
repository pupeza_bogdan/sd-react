import React, { useState, useEffect } from "react";
import {
  Button,
  Card,
  CardHeader,
  Modal,
  ModalBody,
  ModalHeader,
} from "reactstrap";

import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import PersonForm from "./components/person-form";
import * as API_USERS from "./api/person-api";
import AppCard from "../commons/cards/app-card";
import AppCardGroup from "../commons/cards/app-card-group";

function PersonContainer(props) {
  const [isSelected, setIsSelected] = useState(false);
  const [isSelected2, setIsSelected2] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [personToUpdate, setPersonToUpdate] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);

  // Store error status and message in the same object because we don't want
  // to render the component twice (using setError and setErrorStatus)
  // This approach can be used for linked state variables.
  const [error, setError] = useState({ status: 0, errorMessage: null });

  // componentDidMount
  useEffect(() => {
    fetchPersons();
  }, []);

  function fetchPersons() {
    return API_USERS.getPersons((result, status, err) => {
      if (result !== null && status === 200) {
        setTableData((tableData) => result);
        setIsLoaded((isLoaded) => true);
      } else {
        setError((error) => ({ status: status, errorMessage: err }));
      }
    });
  }

  function toggleAddForm() {
    setIsSelected((isSelected) => !isSelected);
  }

  function toggleUpdateForm() {
    console.log("Update item called");
    setIsSelected2((isSelected2) => !isSelected2);
  }

  function reload() {
    setIsLoaded((isLoaded) => false);
    toggleAddForm();
    fetchPersons();
  }

  function reload2() {
    setIsLoaded((isLoaded) => false);
    toggleUpdateForm();
    fetchPersons();
  }

  function reload3() {
    setIsLoaded((isLoaded) => false);
    fetchPersons();
  }

  function deletePerson(id) {
    return API_USERS.deletePerson({ id: id }, (result, status, err) => {
      if (result !== null && status === 200) {
        console.log(result);
        reload3();
      } else {
        setError((error) => ({ status: status, errorMessage: err }));
      }
    });
  }

  return (
    <div>
      <CardHeader>
        <strong> Person Management </strong>
        <Button color="primary" onClick={toggleAddForm}>
          Add Person{" "}
        </Button>
      </CardHeader>
      <Card>
        <AppCardGroup>
          {tableData.map((item, index) => (
            <AppCard
              key={index}
              title={item.name}
              subtitle={item.email}
              items={[
                "id: " + item.id,
                "role: " + item.role,
                "address: " + item.address,
                "age: " + item.age,
              ]}
              link1="Update"
              link2="Delete"
              onLink1Tap={() => {
                setPersonToUpdate(item);
                toggleUpdateForm();
              }}
              onLink2Tap={() => {
                deletePerson(item.id);
              }}
            />
          ))}
        </AppCardGroup>
      </Card>

      <Modal isOpen={isSelected} toggle={toggleAddForm} size="lg">
        <ModalHeader toggle={toggleAddForm}> Add Person: </ModalHeader>
        <ModalBody>
          <PersonForm
            reloadHandler={reload}
            edit={false}
            person={{ name: "", email: "", role: "" }}
          />
        </ModalBody>
      </Modal>

      <Modal isOpen={isSelected2} toggle={toggleUpdateForm} size="lg">
        <ModalHeader toggle={toggleUpdateForm}> Update Person: </ModalHeader>
        <ModalBody>
          <PersonForm
            reloadHandler={reload2}
            edit={true}
            person={personToUpdate}
          />
        </ModalBody>
      </Modal>
    </div>
  );
}

export default PersonContainer;
