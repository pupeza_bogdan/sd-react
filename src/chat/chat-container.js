import React, { useState, useEffect } from "react";
import { Card } from "reactstrap";

import { TalkBox } from "react-talk";
import SockJsClient from "react-stomp";

const WEBSOCKET_URL = "http://localhost:8083/api/v1/chat/websocket";

function ChatContainer() {
  const [currentUserId, setUserId] = useState("");
  const [currentUserName, setUserName] = useState("");
  const [currentUserRole, setUserRole] = useState("");
  const [messages, setMessages] = useState([]);
  const [clientConnected, setClientConnected] = useState(false);
  const [clientRef, setClientRef] = useState("");

  useEffect(() => {
    setUserName(localStorage.getItem("email"));
    setUserId(localStorage.getItem("user-id"));
    setUserRole(localStorage.getItem("user-role"));
    setMessages(
      localStorage.getItem("user-role") === "ROLE_ADMIN"
        ? [
            {
              author: "Any User",
              authorId: "0",
              message: "Help me!",
              timestamp: Date.now().toString(),
            },
          ]
        : [
            {
              author: "Admin",
              authorId: "0",
              message: "Hello! How can we help you?",
              timestamp: Date.now().toString(),
            },
          ]
    );
  }, []);

  let onConnected = () => {
    setClientConnected(true);
  };

  let onMessageReceived = (msg) => {
    if (currentUserRole === "ROLE_ADMIN" && msg.receiver !== undefined) {
      msg.message = "[to: " + msg.receiver + "]\n" + msg.message;
    }
    setMessages([...messages, msg]);
  };

  let userTopics =
    currentUserRole === "ROLE_ADMIN"
      ? [
          "/topic/all",
          "/topic/admin",
          "/queue/admin",
          // "/queue/chat/user/" + localStorage.getItem("user-id"),
        ]
      : [
          "/topic/admin",
          "/queue/user/" + localStorage.getItem("email"),
          //
        ];

  let sendTopic =
    currentUserRole === "ROLE_ADMIN" ? "/app/to_user" : "/app/to_admin";

  let sendMessage = (msg, selfMsg) => {
    console.log("sendTopic: ", sendTopic);
    try {
      selfMsg.author = currentUserName;
      if (currentUserRole === "ROLE_ADMIN" && selfMsg.message[0] === "/") {
        let receiverUsers = selfMsg.message.split(" ")[0].substring(1);
        let actualMessage = selfMsg.message.substring(receiverUsers.length + 2);
        console.log(receiverUsers);
        console.log(actualMessage);
        selfMsg.message = actualMessage;
        selfMsg.receiver = receiverUsers;
      }
      clientRef.sendMessage(sendTopic, JSON.stringify(selfMsg));
      // console.log(clientRef);
      return true;
    } catch (e) {
      return false;
    }
  };

  return (
    <div>
      <Card>
        <TalkBox
          topic="Q & A"
          currentUserId={currentUserId}
          currentUser={currentUserName}
          messages={messages}
          onSendMessage={sendMessage}
          connected={clientConnected}
        />
      </Card>

      <SockJsClient
        url={WEBSOCKET_URL}
        topics={userTopics}
        ref={(client) => {
          setClientRef(client);
        }}
        onConnect={onConnected}
        onDisconnect={console.log("Disconnected!")}
        onMessage={(msg) => onMessageReceived(msg)}
        debug={false}
      />
    </div>
  );
}

export default ChatContainer;
