import { HOST } from "../../commons/hosts";
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
  chat: "/api/v1/chat",
};

function sendMessage(params, callback) {
  let request = new Request(HOST.chat_api + endpoint.chat + "/" + params.id, {
    method: "POST",
    headers: {
      Authorization: "Bearer " + localStorage.getItem("auth-token"),
    },
    body: params.body,
  });
  console.log(request.url);
  RestApiClient.performRequest(request, callback);
}

export { sendMessage };
