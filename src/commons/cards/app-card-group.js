import React from "react";

const AppCardGroup = (props) => {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
        flexWrap: "wrap",
      }}
    >
      {props.children}
    </div>
  );
};

export default AppCardGroup;
