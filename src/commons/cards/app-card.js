import React from "react";

import {
  Button,
  Card,
  CardBody,
  CardTitle,
  CardText,
  ListGroup,
  ListGroupItem,
  NavLink,
} from "reactstrap";

const AppCard = (props) => (
  <Card
    style={{
      width: "30vw",
      margin: "50px 10px 1px 1px",
    }}
  >
    <CardBody>
      <CardTitle tag="h5">{props.title}</CardTitle>
      <CardText>{props.subtitle}</CardText>
    </CardBody>
    <ListGroup flush>
      {props.items.map((item, index) => (
        <ListGroupItem key={index}>{item} </ListGroupItem>
      ))}
    </ListGroup>
    <CardBody>
      <AppButton onClick={props.onLink1Tap} link={props.link1}></AppButton>{" "}
      <AppButton onClick={props.onLink2Tap} link={props.link2}></AppButton>{" "}
      <AppNavLink path={props.link3Path} link={props.link3}></AppNavLink>{" "}
    </CardBody>
  </Card>
);
const AppNavLink = (props) => {
  if (props.link === undefined) {
    return <div></div>;
  } else {
    return (
      <NavLink href={props.path}>
        <Button color="primary" onClick={undefined}>
          {props.link}
        </Button>
      </NavLink>
    );
  }
};
const AppButton = (props) => {
  if (props.link === undefined) {
    return <div></div>;
  } else {
    return (
      <Button color="primary" onClick={props.onClick}>
        {props.link}
      </Button>
    );
  }
};

export default AppCard;
