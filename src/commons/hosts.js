export const HOST = {
  users_api: "http://localhost:8080",
  devices_api: "http://localhost:8081",
  monitoring_api: "http://localhost:8082",
  chat_api: "http://localhost:8083",
};
