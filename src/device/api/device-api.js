import { HOST } from "../../commons/hosts";
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
  device: "/api/v1/devices",
};

function getDevices(callback) {
  let request = new Request(HOST.devices_api + endpoint.device, {
    method: "GET",
    headers: {
      Authorization: "Bearer " + localStorage.getItem("auth-token"),
    },
  });
  console.log(request.url);
  RestApiClient.performRequest(request, callback);
}

function getDeviceById(params, callback) {
  let request = new Request(HOST.devices_api + endpoint.device + params.id, {
    method: "GET",
  });

  console.log(request.url);
  RestApiClient.performRequest(request, callback);
}

function postDevice(device, callback) {
  let request = new Request(HOST.devices_api + endpoint.device, {
    method: "POST",
    headers: {
      Authorization: "Bearer " + localStorage.getItem("auth-token"),
      accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(device),
  });

  console.log("URL: " + request.url);

  RestApiClient.performRequest(request, callback);
}

function patchDevice(params, user, callback) {
  console.log("PARAMS: " + params.id);
  let request = new Request(
    HOST.devices_api + endpoint.device + "/" + params.id,
    {
      method: "PATCH",
      headers: {
        accept: "application/json",
        Authorization: "Bearer " + localStorage.getItem("auth-token"),
        "Content-Type": "application/json",
      },
      body: JSON.stringify(user),
    }
  );

  console.log("URL: " + request.url);

  RestApiClient.performRequest(request, callback);
}

function deleteDevice(params, callback) {
  let request = new Request(
    HOST.devices_api + endpoint.device + "/" + params.id,
    {
      method: "DELETE",
      headers: {
        accept: "application/json",
        Authorization: "Bearer " + localStorage.getItem("auth-token"),
        "Content-Type": "application/json",
      },
    }
  );

  console.log(request.url);
  RestApiClient.performRequest(request, callback);
}

export { getDevices, getDeviceById, postDevice, patchDevice, deleteDevice };
