import React, { useState, useEffect } from "react";
import {
  Button,
  Card,
  CardHeader,
  Modal,
  ModalBody,
  ModalHeader,
} from "reactstrap";

import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import DeviceForm from "./components/device-form";
import * as API_DEVICES from "./api/device-api";
import AppCard from "../commons/cards/app-card";
import AppCardGroup from "../commons/cards/app-card-group";

function DeviceContainer(props) {
  const [isSelected, setIsSelected] = useState(false);
  const [isSelected2, setIsSelected2] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [deviceToUpdate, setDeviceToUpdate] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);

  // Store error status and message in the same object because we don't want
  // to render the component twice (using setError and setErrorStatus)
  // This approach can be used for linked state variables.
  const [error, setError] = useState({ status: 0, errorMessage: null });

  // componentDidMount
  useEffect(() => {
    fetchDevices();
  }, []);

  function fetchDevices() {
    return API_DEVICES.getDevices((result, status, err) => {
      if (result !== null && status === 200) {
        setTableData((tableData) => result);
        setIsLoaded((isLoaded) => true);
      } else {
        setError((error) => ({ status: status, errorMessage: err }));
      }
    });
  }

  function deleteDevice(deviceId) {
    return API_DEVICES.deleteDevice({ id: deviceId }, (result, status, err) => {
      if (result !== null && (status === 200 || status === 201)) {
        console.log("Successfully deleted device with id: " + result);
        fetchDevices();
      } else {
        setError((error) => ({ status: status, errorMessage: err }));
      }
    });
  }

  function toggleAddForm() {
    setIsSelected((isSelected) => !isSelected);
  }

  function toggleUpdateForm() {
    console.log("Update item called");
    setIsSelected2((isSelected2) => !isSelected2);
  }

  function reload() {
    setIsLoaded((isLoaded) => false);
    toggleAddForm();
    fetchDevices();
  }

  function reload2() {
    setIsLoaded((isLoaded) => false);
    toggleUpdateForm();
    fetchDevices();
  }

  return (
    <div>
      <CardHeader>
        <strong> Device Management </strong>
        <Button color="primary" onClick={toggleAddForm}>
          Add Device{" "}
        </Button>
      </CardHeader>
      <Card>
        <AppCardGroup>
          {tableData.map((item, index) => (
            <AppCard
              key={index}
              title={"Device: " + item.id}
              subtitle={item.description}
              items={[
                "user id: " + item.userId,
                "mhec: " + item.mhec,
                "address: " + item.address,
              ]}
              link1="Update"
              link2="Delete"
              link3="Monitoring"
              onLink1Tap={() => {
                setDeviceToUpdate(item);
                toggleUpdateForm();
              }}
              onLink2Tap={() => {
                deleteDevice(item.id);
              }}
              link3Path={"/monitoring/" + item.id}
            />
          ))}
        </AppCardGroup>
      </Card>

      <Modal isOpen={isSelected} toggle={toggleAddForm} size="lg">
        <ModalHeader toggle={toggleAddForm}> Add Device: </ModalHeader>
        <ModalBody>
          <DeviceForm
            reloadHandler={reload}
            edit={false}
            device={{ description: "", mhec: "", address: "", userId: "" }}
          />
        </ModalBody>
      </Modal>

      <Modal isOpen={isSelected2} toggle={toggleUpdateForm} size="lg">
        <ModalHeader toggle={toggleUpdateForm}> Update Device: </ModalHeader>
        <ModalBody>
          <DeviceForm
            reloadHandler={reload2}
            edit={true}
            device={deviceToUpdate}
          />
        </ModalBody>
      </Modal>
    </div>
  );
}

export default DeviceContainer;
