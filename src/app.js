import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";

import NavigationBar from "./navigation-bar";
import Home from "./home/home";
import PersonContainer from "./person/person-container";
import DeviceContainer from "./device/device-container";
import LoginContainer from "./login/login-container";
import ErrorPage from "./commons/errorhandling/error-page";
import styles from "./commons/styles/project-style.css";
import PrivateRoute from "./commons/routes/private-route";
import MonitoringChart from "./monitoring/components/monitoring-chart";
import ChatContainer from "./chat/chat-container";

/*
    Namings: https://reactjs.org/docs/jsx-in-depth.html#html-tags-vs.-react-components
    Should I use hooks?: https://reactjs.org/docs/hooks-faq.html#should-i-use-hooks-classes-or-a-mix-of-both
*/
function App() {
  return (
    <div className={styles.back}>
      <Router>
        <div>
          <NavigationBar />
          <Switch>
            <Route exact path="/" render={() => <Home />} />

            <PrivateRoute path="/person" component={PersonContainer} />

            <PrivateRoute path="/device" component={DeviceContainer} />

            <PrivateRoute
              path="/monitoring/:deviceId"
              component={MonitoringChart}
            />

            <PrivateRoute path="/chat" component={ChatContainer} />

            <Route exact path="/login" render={() => <LoginContainer />} />
            {/*Error*/}
            <Route exact path="/error" render={() => <ErrorPage />} />

            <Route render={() => <ErrorPage />} />
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
