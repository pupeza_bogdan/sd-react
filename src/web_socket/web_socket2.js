import React, { useState } from "react";
import Popup from "reactjs-popup";
import { Card, Container } from "reactstrap";
import { StompSessionProvider, useSubscription } from "react-stomp-hooks";

const WEBSOCKET_URL = "http://localhost:8082/api/v1/monitoring/websocket";

// WebSocket at server is configured with SockJs

const AppWebsocket = () => {
  var clientRef = "";
  const [messageList, setMessage] = useState([
    "Notifications will be displayed here.",
  ]);
  let onConnected = () => {
    console.log("Connected!");
  };

  let onMessageReceived = (msg) => {
    console.log("Message: " + msg.content);
    setMessage(messageList.concat("Device status: " + msg.content));
  };
  return (
    <Container>
      <StompSessionProvider
        url={WEBSOCKET_URL}
        topics={["/topic/monitoring"]}
        // onConnect={onConnected}
        // onDisconnect={console.log("Disconnected!")}
        // ref={(client) => {
        //   clientRef = client;
        //   console.log("ClientRef: " + clientRef);
        // }}
        // onMessage={(msg) => onMessageReceived(msg)}
        // debug={true}
      >
        {" "}
        <ChildComponent />
      </StompSessionProvider>
    </Container>
  );
};

const ChildComponent = () => {
  const [message, setMessage] = useState("");
  useSubscription("/queue/reply-user1", (message) => {
    console.log(message.body);
    setMessage(message.body);
  });

  return (
    <Popup
      trigger={<div style={{ color: "white" }}>Notifications</div>}
      position="bottom left"
    >
      <Card>
        <div style={{ overflow: "scroll", height: "250px" }}>
          {"The message is:\n" + message}
        </div>
      </Card>
    </Popup>
  );
};

// const PublishComponent = () => {
//   const stompClient = useStompClient();

//   const publishMessage = () => {
//     if (stompClient) {
//       stompClient.publish({
//         destination: "/app/broadcast",
//         body: "Hello World",
//       });
//     }
//   };
//   return <div onClick={publishMessage}> Send message </div>;
// };
export default AppWebsocket;
