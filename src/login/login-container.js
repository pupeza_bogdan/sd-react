import React, { useState, useEffect } from "react";
import {
  FormGroup,
  Label,
  Input,
  Container,
  Button,
  Row,
  Col,
} from "reactstrap";

import * as API_USERS from "./api/login-api";
import { useHistory } from "react-router-dom";

const formControlsInit = {
  email: {
    value: "",
    placeholder: "email",
    valid: false,
    touched: false,
    validationRules: {
      minLength: 3,
      isRequired: true,
    },
  },
  password: {
    value: "",
    placeholder: "password",
    valid: false,
    touched: false,
    validationRules: {
      minLength: 3,
      isRequired: true,
    },
  },
};

// const [error, setError] = useState({ status: 0, errorMessage: null });

const LoginContainer = (props) => {
  const history = useHistory();

  const [formControls, setFormControls] = useState(formControlsInit);
  const [formIsValid, setFormIsValid] = useState(false);

  function handleChange(event) {
    let name = event.target.name;
    let value = event.target.value;

    let updatedControls = { ...formControls };

    let updatedFormElement = updatedControls[name];

    updatedFormElement.value = value;
    updatedFormElement.touched = true;
    updatedFormElement.valid = true;
    // Validate(
    //   value,
    //   updatedFormElement.validationRules
    // );
    updatedControls[name] = updatedFormElement;

    let formIsValid = true;
    for (let updatedFormElementName in updatedControls) {
      formIsValid =
        updatedControls[updatedFormElementName].valid && formIsValid;
    }

    setFormControls((formControls) => updatedControls);
    setFormIsValid((formIsValidPrev) => formIsValid);
  }

  function handleSubmit() {
    let loginData = {
      email: formControls.email.value,
      password: formControls.password.value,
    };
    registerPerson(loginData);
  }

  function registerPerson(loginData) {
    console.log("Try to login: " + loginData.email + "/" + loginData.password);
    return API_USERS.login(loginData, (result, status, err) => {
      if (result !== null && (status === 200 || status === 201)) {
        console.log("Result: " + result.token);
        localStorage.setItem("auth-token", result.token);
        localStorage.setItem("user-id", result.userId);
        localStorage.setItem("user-role", result.userRole);
        localStorage.setItem("email", loginData.email);
        history.push("/");
        // props.reloadHandler();
      } else {
        console.log("Status:" + status + " Error:" + err);
        // setError((error) => ({ status: status, errorMessage: err }));
      }
    });
  }

  return (
    <Container>
      <FormGroup id="email">
        <Label for="emailField"> Email: </Label>
        <Input
          name="email"
          id="emailField"
          placeholder={formControls.email.placeholder}
          onChange={handleChange}
          defaultValue={formControls.email.value}
          touched={formControls.email.touched ? 1 : 0}
          valid={formControls.email.valid}
          required
        />
        {formControls.email.touched && !formControls.email.valid && (
          <div className={"error-message row"}>
            {" "}
            * Name must have at least 3 characters{" "}
          </div>
        )}
      </FormGroup>

      <FormGroup id="password">
        <Label for="passwordField"> Password: </Label>
        <Input
          name="password"
          id="passwordField"
          placeholder={formControls.password.placeholder}
          onChange={handleChange}
          defaultValue={formControls.password.value}
          touched={formControls.password.touched ? 1 : 0}
          valid={formControls.password.valid}
          required
        />
        {formControls.password.touched && !formControls.password.valid && (
          <div className={"error-message row"}>
            {" "}
            * Name must have at least 3 characters{" "}
          </div>
        )}
      </FormGroup>
      <Row>
        <Col sm={{ size: "4", offset: 8 }}>
          <Button
            type={"submit"}
            disabled={!formIsValid}
            onClick={handleSubmit}
          >
            Submit
          </Button>
        </Col>
      </Row>
    </Container>
  );
};

export default LoginContainer;
