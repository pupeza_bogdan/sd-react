import React from "react";
import {
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  Navbar,
  NavbarBrand,
  NavLink,
  UncontrolledDropdown,
  Button,
} from "reactstrap";

import logo from "./commons/images/icon.png";
import AppWebsocket from "./web_socket/web_socket";

const textStyle = {
  color: "white",
  textDecoration: "none",
};

function NavigationBar() {
  return (
    <div>
      <Navbar color="dark" light expand="md">
        <NavbarBrand href="/">
          <img src={logo} width={"50"} height={"35"} />
        </NavbarBrand>
        <Nav className="mr-auto" navbar>
          <UncontrolledDropdown nav inNavbar>
            <DropdownToggle style={textStyle} nav caret>
              Menu
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem>
                <NavLink href="/person">Persons</NavLink>
              </DropdownItem>
              <DropdownItem>
                <NavLink href="/device">Devices</NavLink>
              </DropdownItem>{" "}
              <DropdownItem>
                <NavLink href="/chat">Chat</NavLink>
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
          <AuthButton />
          <AppWebsocket />
        </Nav>
      </Navbar>
    </div>
  );
}

//maybe useState here
const AuthButton = () => {
  if (localStorage.getItem("auth-token") === null) {
    return (
      <NavLink href="/login">
        <Button color="primary">Login </Button>
      </NavLink>
    );
  } else {
    return (
      <NavLink href="/">
        <Button color="primary" onClick={logout}>
          Logout{" "}
        </Button>
      </NavLink>
    );
  }
};

function logout() {
  localStorage.clear();
}

export default NavigationBar;
